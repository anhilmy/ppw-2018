from django.db import models
from django.utils import timezone

class Post(models.Model):
    objects = models.Manager()
    createTime = models.DateTimeField(default = timezone.now)
    title = models.CharField(default="", max_length = 30)
    content = models.CharField(default="I like dancing", max_length = 300)

    def __str__(self):
	    return self.title

