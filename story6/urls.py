from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path("", views.forms6, name = "home"),
    path("profilePage/", views.profile_Page, name="profile"),
    path("deleteAll/", views.deleteAll, name="deleteAll"),
    path("favoritePage/", views.favoritePage, name="favorit"),
    path("favoritePage/bookData/<search>", views.bookData, name="data_buku_search"),
    path("favoritePage/bookData/", views.bookData, name="data_buku"),
]
