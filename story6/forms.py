from django import forms
from datetime import datetime
import time
# from .models import Schedule

class Post_Form(forms.Form):

    title_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Judul', 
	'id' : 'judul',
    }
    content_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Isi',
	'id':'isi',
    }


    title = forms.CharField(label = "Judul", required=False, max_length=30,widget=forms.TextInput(attrs=title_attrs))
    content = forms.CharField(label = "Status", required=True, max_length=300, widget=forms.Textarea(attrs=content_attrs))
