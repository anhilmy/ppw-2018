from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from . import templates
from .models import Post
from .forms import Post_Form
from datetime import datetime
import requests
import json
from django.http import JsonResponse
import time

def forms6(request):
    response = {}
    response['forms'] = Post_Form()
    response['post'] = Post.objects.all()
    if(request.method == "POST"):
        title = request.POST['title']
        content = request.POST['content']
        published = datetime.now()
        post = Post(title=title, content=content, createTime = published)
        post.save()
    return render(request, 'story6Home.html', response)

def profile_Page(request):
    return render(request, 'profilePage.html')

def deleteAll(request):
    Post.objects.all().delete()
    return HttpResponseRedirect('..')

#TODO challenge story9, bikin 2 def buat nyimpen variable inputnya terus refresh page
#TODO terus refresh page bakal inisiasi dataTable lagi dengan input yang diberikan
	
def bookData(request):
    string_url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET["search"]
    print(string_url)
    url = requests.get(string_url)
    json_data = url.json()['items']
    response1 = {'data':json_data}
    return JsonResponse(response1)
    

def favoritePage(request):
    return render(request, "favoritPage.html")

