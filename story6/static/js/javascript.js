
$(function () {
    $("#accordion").accordion({
        collapsible: true,
        heightStyle: "content",
    });
});

$(document).ready(function () {
    $("#change").click(function () {
        $("body").css({ "background-color": "black" });
        $(".header").css({ "color": "whitesmoke" });
        $("body").css({ "color": "whitesmoke" });
        $("h4").css({ "color": "whitesmoke" });
        $("a").css({ "color": "whitesmoke" });
        $("h7").css({ "color": "whitesmoke" });

    });
});

// $(document).ready(function() {
//     $("#cari_buku_submit").click(function(){
//         var input = $("#cari_buku").val;
//         $.ajax({
//             url = "dataBook/",

//         });
//     })
// });

// var myVar;

// function waitFunction() {
//     myVar = setTimeout(showPage, 1000);
// }

// function showPage() {
//     document.getElementById("loader").style.display = "none";
//     document.getElementById("myDiv").style.display = "block";
// }
// $.post("demo_test_post.asp",
//     {
//         name: "Donald Duck",
//         city: "Duckburg"
//     },
//     function(data, status){
//         alert("Data: " + data + "\nStatus: " + status);
//     });
// datatables plugin for render books in table
var keyword = "quilting"

$(document).ready(function () {
    $("#cari_buku_submit").click(function(){
        keyword = $("#cari_buku").val();
        console.log("VAL IS CHANGED TO INPUT");

    $('#dataBuku').DataTable({
            "scrollX": true,
            "ajax": {
                data:{
                    search:keyword
                },
                url: "bookData/",
            },
    
            "lengthMenu": [5, 10, 25, 50],
    
            "columns": [
    
                {
                    // image thumbnail
                    data: null,
                    render: function (data, type, row, meta) {
                        return '<img src=' + data.volumeInfo.imageLinks.smallThumbnail + '>'
    
                    }
                },
                {
                    data: "volumeInfo.title",
                    defaultContent: "<i>unknown data<i>"
                },
                {
                    data: "volumeInfo.authors[,]",
                    defaultContent: "<i>unknown data<i>"
                },
                {
                    data: "volumeInfo.publishedDate",
                    defaultContent: "<i>unknown data<i>"
                },
                {
                    data: "volumeInfo.categories",
                    defaultContent: "<i>unknown data<i>"
                },
                {
                    orderable: false,
                    data: null,
    
                    // passing data id
                    render: function (data, type, row, meta) {
                        return "<p class='favorite star-symbol' style='font-size:50px; text-align:center; cursor: pointer '>" + "☆" + "</p>"
                    }
    
                }
    
            ],
        });
    });
    
        // thank you cloud for giving me the star pict :)
        var count_books = localStorage.getItem('favorite-count');
    
        if (count_books === null) {
            count_books = 0;
        }
    
        update_favorite();
    
        $(document).on('click', '.favorite', function () {
            if ($(this).text() == "☆") {
                count_books++;
                update_favorite();
                $(this).text("★").css('color', 'yellow');
            } else {
                count_books--;
                update_favorite();
                $(this).text("☆").css('color', 'black');
            }
        });
    
        localStorage.setItem('favorite-count', count_books);
    
        function update_favorite() {
            document.getElementById('favorite-count').innerHTML = count_books;
        };
    

     });
