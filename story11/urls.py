from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path("", views.dummyHome, name = "dummyHome"),
    # path("login/", views.login, name="login"),
    path("dummyAuth/", views.dummyauth, name="dummylogin"),
    url(r'^auth/', include('social_django.urls', namespace='social')), 
    path('logout/', views.logoutF,  name='logout'),
    path('add_click/', views.add_click, name="add_clicker"),
    # path("del_session/<int:id>", views.del_session, name="delete_session"),

]
