from datetime import datetime
import time
from django.forms import ModelForm
from django import forms

class dummyForm(forms.Form):
    name = forms.CharField(
        label="Username", 
        required=True, 
        max_length=999,
        widget=forms.TextInput(attrs={
            "type" : "text",
            "class" : "form-control js-username",
            "id":"username",
            "placeholder": "Username",
        }))

# class Subscribe_Form(ModelForm):
#     class Meta:
#         model = User
#         fields = ["username", "email", "password"]

#         widgets = {
#             "username": forms.TextInput(
#                 attrs = {
#                     "class":"form-control",
#                     "type":"text",
#                     "placeholder":"Username",
#                     "id":"id_username"
#                 }),
#             "email": forms.EmailInput(
#                 attrs = {
#                     "class":"form-control",
#                     "type":"email",
#                     "placeholder":"E-mail",
#                     "id":"id_email"
#                 }
#             ),
#             "password": forms.PasswordInput(
#                 attrs = {
#                     "class":"form-control",
#                     "type":"password",
#                     "placeholder":"",
#                     "id":"id_password"
#                 }
#             ),
#         }