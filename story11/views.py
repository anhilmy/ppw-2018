from .forms import dummyForm
from django.shortcuts import render, redirect
from social_django.models import UserSocialAuth
from django.contrib.auth import logout

def dummyauth(request):
    return render(request, "dummyAuth.html")

def logoutF(request):
    logout(request)
    return redirect("/")

def dummyHome(request):
    form_login = dummyForm()
    user = request.user
    username = user.username

    if(username == ""):
        print("USERNAME IS ANONYMOUS USER")
        username = None
        
    return render(request, 'login.html', {
        'forms': form_login,
        'username': username,
    })

def add_click(request):
    request.session["num"] = num = 0
    if(num == 0):
        request.session["num"] = 1

    else:
        request.session["num"] += 1

    return JsonResponse(request.session)