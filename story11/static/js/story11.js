$(document).ready(function () {
    var username = ""
    var email = ""
    var password = ""

    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('X-CSRFToken', csrftoken);
        }
    });

    $('#subscribe_form').on('submit', function (event) {
        event.preventDefault();
        console.log("form submitted!")  // sanity check
        create_user();
    });

    $("#id_username").change(function () {
        username = $(this).val();
        $.ajax({
            url: '/validateForm',
            data: {
                'username': username,
                'email': email,
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken_username) {
                    alert("A user with this username already exists.");
                }
            }
        });
        console.log($(this).val());
    });

    $("#id_email").change(function () {
        email = $(this).val();
        $.ajax({
            url: '/validateForm',
            data: {
                'username': username,
                'email': email,
            },
            dataType: 'json',
            success: function (data) {
                if (data.is_taken_email) {
                    alert("A user with this email already exists.");
                }
            }
        });
        console.log($(this).val());
    });

    $("#id_password").change(function () {
        password = $(this).val();
    });

    function create_user() {
        console.log("creating user");
        // $.ajaxSetup({
        //     beforeSend: function (xhr) {
        //         xhr.setRequestHeader('csrf-token', csrftoken);
        //     }
        // });
        $.ajax({
            url: '{% url "add_user" %}', // the endpoint
            type: "POST", // http method
            data: {
                "username": username,
                "email": email,
                "password": password
            }, // data sent with the post request
            // handle a successful response
            success: function () {
                console.log("MASUK SINI")
                $('#id_username').val(''); // remove the value from the input
                $('#id_password').val(''); // remove the value from the input
                $('#id_email').val(''); // remove the value from the input
                // console.log(json); // log the returned json to the console
                console.log("success"); // another sanity check
            },

            // handle a non-successful response
            // error: function (xhr, errmsg, err) {
            //     $('#results').html("<div class='alert-box alert radius' data-alert>Oops! We have encountered an error: " + errmsg +
            //         " <a href='#' class='close'>&times;</a></div>"); // add the error to the dom
            //     console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            // }
        });
    };
});    