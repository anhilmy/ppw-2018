from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from . import templates
from .models import User
from .forms import Subscribe_Form
from datetime import datetime
import requests
import json
from django.http import JsonResponse
import time

def home(request):
    response = {}
    response['forms'] = Subscribe_Form()
    return render(request, 'subscribePage.html', response)

def addUser(request):
    print("MASUK SINIIIIII")
    print("MASUK SINIIIIII")
    print("MASUK SINIIIIII")
    print("MASUK SINIIIIII")
    print("MASUK SINIIIIII")
    print("MASUK SINIIIIII")
    response = {}
    if(request.method == "POST"):
        response['username'] = request.POST["username"]
        response['email'] = request.POST["email"]
        response['password'] = request.POST["password"]
        obj = User(username=response['username'], email = response['email'], password=response['password'])
        obj.save()
        all_obj = User.objects.all()
        print(all_obj)
    return JsonResponse(response)

def validate(request):
    username = request.GET.get('username', None)
    email = request.GET.get('email', None)
    data = {
        'is_taken_username': User.objects.filter(username__iexact=username).exists(),
        'is_taken_email':User.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)