from datetime import datetime
import time
from .models import User
from django.forms import ModelForm
from django import forms

class Subscribe_Form(ModelForm):
    class Meta:
        model = User
        fields = ["username", "email", "password"]

        widgets = {
            "username": forms.TextInput(
                attrs = {
                    "class":"form-control",
                    "type":"text",
                    "placeholder":"Username",
                    "id":"id_username"
                }),
            "email": forms.EmailInput(
                attrs = {
                    "class":"form-control",
                    "type":"email",
                    "placeholder":"E-mail",
                    "id":"id_email"
                }
            ),
            "password": forms.PasswordInput(
                attrs = {
                    "class":"form-control",
                    "type":"password",
                    "placeholder":"",
                    "id":"id_password"
                }
            ),
        }
    




# from django import forms
# from .models import pendaftaran_donatur_model
# from django.forms import ModelForm


# class pendaftaran_donatur_forms(ModelForm):
#     class Meta:
#         model = pendaftaran_donatur_model
#         # isi dengan fields yang ada di models.py
#         fields = ['nama_donatur_field', 'tanggal_lahir_field', 'email_field', 'password_field']

#         widgets = {
#             'nama_donatur_field': forms.TextInput(
#                 attrs={'class': 'form-control ', 'type': 'text', 'placeholder': 'Isi dengan nama anda',
#                        'background-color': 'black;'}),
#             'tanggal_lahir_field': forms.DateInput(
#                 attrs={'class': 'form-control', 'type': 'date'}),
#             'email_field': forms.EmailInput(
#                 attrs={'class': 'form-control', 'type': 'email', 'placeholder': 'Isi dengan email valid anda'}),
#             'password_field': forms.PasswordInput(
#                 attrs={'class': 'form-control', 'type': 'password', 'placeholder': 'Isi dengan password yang unik'}
#             ),
#         }

#         labels = {
#             'nama_donatur_field': 'Nama',
#             'tanggal_lahir_field': 'Tanggal Lahir',
#             'email_field': 'E-mail',
#             'password_field': 'Password',
#         }

#     # def clean_email_field(self):
#     #     email = self.cleaned_data['email_field']
#     #     try:
#     #         object_email = pendaftaran_donatur_model.get(email_field=email)
#     #         raise forms.ValidationError("Email sudah terdaftar!")
#     #     except not (forms.ValidationError):
#     #         return email
