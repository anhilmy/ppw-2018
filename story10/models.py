from django.db import models

class User(models.Model):
    username = models.CharField(max_length=30)
    email = models.EmailField(max_length=70)
    password = models.CharField(max_length=64)

    def __str__(self):
        return self.username
# Create your models here.
