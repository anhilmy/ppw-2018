from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path("", views.home, name = "home"),
    path("addUser/", views.addUser, name="add_user"),
    path("validateForm/", views.validate, name="name_validate")
]
