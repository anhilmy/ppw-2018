from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField('auth.User', on_delete=models.CASCADE)
    username = models.CharField(max_length = 64)
    email = models.EmailField()
    photourls = models.CharField(max_length = 300)

    def __str__(self):
        return self.username

def save_profile(response, user, *args, **kwargs):
    #kalau belum ada create
    emailMasuk = response["emails"][0]["value"]
    ada = UserProfile.objects.filter(email=emailMasuk).count()
    if((ada == 0)):
        UserProfile.objects.create(
            user = user,
            username = response["displayName"],
            email = emailMasuk,
            photourls = response["image"]["url"]
        )
        
